-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2014 at 12:34 AM
-- Server version: 5.5.37-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pencitra_simentor`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas`
--

CREATE TABLE IF NOT EXISTS `aktivitas` (
  `id_aktivitas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_aktivitas` varchar(50) NOT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_aktivitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `aktivitas`
--

INSERT INTO `aktivitas` (`id_aktivitas`, `nama_aktivitas`, `is_delete`) VALUES
(1, 'Sholat', 1),
(2, 'Kehadiran', 1),
(3, 'Kajian', 1),
(4, 'Mengisi Halaqah', 1),
(5, 'Mabit', 1),
(6, 'Rihlah', 1),
(7, 'Shalat Jamaah', 0),
(8, 'Tilawah', 0),
(9, 'QL', 1),
(10, 'Al-Ma''surat', 1),
(11, 'Shaum Sunnah', 0),
(12, 'Shalat Dhuha', 0),
(13, 'Hafalan Al-Qur''an', 0),
(14, 'Haji', 1);

-- --------------------------------------------------------

--
-- Table structure for table `arsip`
--

CREATE TABLE IF NOT EXISTS `arsip` (
  `id_arsip` int(11) NOT NULL AUTO_INCREMENT,
  `id_binaan` int(11) NOT NULL,
  `nama_binaan` varchar(30) NOT NULL,
  `fakultas_binaan` varchar(50) NOT NULL,
  `jurusan_binaan` varchar(50) NOT NULL,
  `angkatan_binaan` varchar(10) NOT NULL,
  `no_hp_binaan` varchar(20) NOT NULL,
  `nama_kelompok` varchar(50) NOT NULL,
  `tingkatan` int(1) NOT NULL,
  PRIMARY KEY (`id_arsip`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `arsip`
--

INSERT INTO `arsip` (`id_arsip`, `id_binaan`, `nama_binaan`, `fakultas_binaan`, `jurusan_binaan`, `angkatan_binaan`, `no_hp_binaan`, `nama_kelompok`, `tingkatan`) VALUES
(5, 3, 'C', 'FKM', 'Gizi', '2012', '87543689', 'Wawan_FKM_1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `evaluasi`
--

CREATE TABLE IF NOT EXISTS `evaluasi` (
  `id_evaluasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_laporan` int(11) NOT NULL,
  `id_aktivitas` int(11) NOT NULL,
  `id_binaan` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  PRIMARY KEY (`id_evaluasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE IF NOT EXISTS `fakultas` (
  `id_fakultas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id_fakultas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `nama`) VALUES
(1, 'FK'),
(2, 'FKG'),
(3, 'Vokasi'),
(4, 'Fasilkom');

-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

CREATE TABLE IF NOT EXISTS `kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_murabbi` int(11) NOT NULL,
  `nama_murabbi` varchar(50) NOT NULL,
  `fakultas_murabbi` varchar(50) NOT NULL,
  `angkatan_murabbi` int(4) NOT NULL,
  `no_hp_murabbi` varchar(20) NOT NULL,
  `email_murabbi` varchar(50) NOT NULL,
  `nama_kelompok` varchar(50) NOT NULL,
  `id_binaan` int(11) NOT NULL,
  `nama_binaan` varchar(50) NOT NULL,
  `fakultas_binaan` varchar(50) NOT NULL,
  `jurusan_binaan` varchar(50) NOT NULL,
  `angkatan_binaan` int(4) NOT NULL,
  `no_hp_binaan` varchar(20) NOT NULL,
  `tingkatan` int(1) NOT NULL,
  `is_kelompok_deleted` int(1) NOT NULL DEFAULT '0',
  `is_binaan_deleted` int(1) NOT NULL DEFAULT '0',
  `is_binaan_promote` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `kelompok`
--

INSERT INTO `kelompok` (`id`, `id_murabbi`, `nama_murabbi`, `fakultas_murabbi`, `angkatan_murabbi`, `no_hp_murabbi`, `email_murabbi`, `nama_kelompok`, `id_binaan`, `nama_binaan`, `fakultas_binaan`, `jurusan_binaan`, `angkatan_binaan`, `no_hp_binaan`, `tingkatan`, `is_kelompok_deleted`, `is_binaan_deleted`, `is_binaan_promote`) VALUES
(1, 46, 'Bang Camat', 'FKG', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_1', 1, 'A', 'FKM', 'Kesmas', 2012, '80999908', 2, 0, 1, 0),
(2, 46, 'Bang Camat', 'FKG', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_1', 2, 'B', 'FKM', 'Kesmas', 2012, '878987966', 2, 0, 1, 0),
(3, 46, 'Bang Camat', 'FKG', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_1', 3, 'C', 'FKM', 'Gizi', 2012, '87543689', 2, 0, 1, 0),
(4, 46, 'Bang Camat', 'FKG', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_1', 4, 'D', 'FKM', 'Gizi', 2012, '865422456', 2, 0, 0, 0),
(5, 47, 'Andreas Senjaya', 'Fasilkom', 2009, '878934562', 'anto@ui.ac.id', 'Anto_Fasilkom_1', 5, 'E', 'Fasilkom', 'IK', 2011, '783654238', 2, 0, 0, 0),
(6, 47, 'Andreas Senjaya', 'Fasilkom', 2009, '878934563', 'anto@ui.ac.id', 'Anto_Fasilkom_1', 6, 'F', 'Fasilkom', 'IK', 2011, '897628172', 2, 0, 0, 0),
(7, 47, 'Andreas Senjaya', 'Fasilkom', 2009, '878934564', 'anto@ui.ac.id', 'Anto_Fasilkom_1', 7, 'G', 'Fasilkom', 'SI', 2011, '896578432', 2, 0, 0, 0),
(8, 47, 'Andreas Senjaya', 'Fasilkom', 2009, '878934565', 'anto@ui.ac.id', 'Anto_Fasilkom_1', 8, 'H', 'Fasilkom', 'SI', 2011, '878653421', 2, 0, 0, 0),
(9, 48, 'Topan Bayu', 'FKM', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_2', 9, 'I', 'FKG', 'KG', 2011, '21', 2, 0, 0, 0),
(10, 48, 'Topan Bayu', 'FKM', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_2', 10, 'J', 'FKG', 'KG', 2011, '22', 2, 0, 0, 0),
(11, 48, 'Topan Bayu', 'FKM', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_2', 11, 'K', 'FKG', 'KG', 2011, '23', 2, 0, 0, 0),
(12, 48, 'Topan Bayu', 'FKM', 2010, '809888888', 'wawan@ui.ac.id', 'Wawan_FKM_2', 12, 'L', 'FKG', 'KG', 2011, '24', 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE IF NOT EXISTS `laporan` (
  `id_laporan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelompok` varchar(50) NOT NULL,
  `id_murabbi` int(5) NOT NULL,
  `tanggal` date NOT NULL,
  `pembukaan` int(1) NOT NULL DEFAULT '0',
  `tilawah` int(1) NOT NULL,
  `kalimat_MR` int(1) NOT NULL,
  `infaq` int(1) NOT NULL,
  `kultum` int(1) NOT NULL,
  `materi` text NOT NULL,
  `mutaba_ah` int(1) NOT NULL,
  `taklimat` int(1) NOT NULL,
  `penutup` int(1) NOT NULL,
  PRIMARY KEY (`id_laporan`),
  KEY `id_laporan` (`id_laporan`),
  KEY `id_laporan_2` (`id_laporan`),
  KEY `id_laporan_3` (`id_laporan`),
  KEY `id_laporan_4` (`id_laporan`),
  KEY `id_laporan_5` (`id_laporan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id_laporan`, `nama_kelompok`, `id_murabbi`, `tanggal`, `pembukaan`, `tilawah`, `kalimat_MR`, `infaq`, `kultum`, `materi`, `mutaba_ah`, `taklimat`, `penutup`) VALUES
(2, 'Wawan_FKM_1', 9, '0000-00-00', 1, 1, 1, 1, 1, 'Sholat', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE IF NOT EXISTS `materi` (
  `id_materi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_materi` varchar(50) NOT NULL,
  `tingkatan` enum('1','2') NOT NULL,
  `path` text NOT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_materi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id_materi`, `nama_materi`, `tingkatan`, `path`, `is_delete`) VALUES
(11, 'Jerusalem', '1', 'file_uploaded/Jerusalem_dalam_Al-Quran_-_Bahasa_Indonesia_Translation.pdf', 1),
(13, 'jerussalem', '1', 'file_uploaded/Jerusalem_dalam_Al-Quran_-_Bahasa_Indonesia_Translation.pdf', 0),
(16, 'asdfasdfasdf', '1', 'file_uploaded/Jerusalem_dalam_Al-Quran_-_Bahasa_Indonesia_Translation.pdf', 0),
(18, 'Hahahhaa', '1', 'file_uploaded/analyst_pengumuman.pdf', 0),
(19, 'Mentoring Yuk', '1', 'file_uploaded/Jadwal_Ujian_Semester_6.pdf', 1),
(20, 'Jadwal', '1', 'file_uploaded/Jadwal.pdf', 0),
(21, 'trililili', '2', 'file_uploaded/<', 1),
(22, 'Jadwal', '2', 'file_uploaded/Jadwal_Ujian_Semester_6.pdf', 1),
(23, '0', '2', 'file_uploaded/<', 0),
(24, 's2', '2', 'file_uploaded/<', 1),
(25, 'pkm', '1', 'file_uploaded/pkm-gt-10-um-windy-penerapan-metode-polimerisasi.pdf', 0),
(26, 'Jadwal', '2', 'file_uploaded/Jadwal.pdf', 0),
(27, 'Coba materi', '2', 'file_uploaded/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id_pesan` int(5) NOT NULL AUTO_INCREMENT,
  `id_thread` int(5) NOT NULL,
  `id_pengirim` int(5) NOT NULL,
  `id_penerima` int(5) NOT NULL,
  `judul_pesan` varchar(50) NOT NULL,
  `isi_pesan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_read` int(1) NOT NULL DEFAULT '0',
  `is_delete_pengirim` int(1) NOT NULL DEFAULT '0',
  `is_delete_penerima` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pesan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id_request` int(10) NOT NULL AUTO_INCREMENT,
  `id_binaan` int(10) NOT NULL,
  `tingkatan` int(1) NOT NULL,
  `is_approve` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` enum('Admin Mentor','Mentor','Admin Murobbi','Murobbi') NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `foto_path` text NOT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `Nama`, `username`, `password`, `role`, `id_fakultas`, `foto_path`, `is_delete`) VALUES
(7, 'Anisa', 'Anisa', 'e172dd95f4feb21412a692e73929961e', 'Admin Mentor', 2, '', 0),
(9, 'Wahyu', 'Wahyu', 'e172dd95f4feb21412a692e73929961e', 'Admin Murobbi', 4, '', 0),
(45, 'hana', 'hana', 'e172dd95f4feb21412a692e73929961e', 'Murobbi', 1, '', 0),
(46, 'Bang Camat', 'Bang62', 'e172dd95f4feb21412a692e73929961e', 'Murobbi', 0, '', 0),
(47, 'Andreas Senjaya', 'Andreas65', 'e172dd95f4feb21412a692e73929961e', 'Murobbi', 0, '', 0),
(48, 'Topan Bayu', 'Topan67', 'e172dd95f4feb21412a692e73929961e', 'Murobbi', 0, '', 0),
(49, 'Ryan', 'ryan.riandi', '873451a384de3e5e13e95ad043e9fcef', 'Admin Murobbi', 4, '', 0),
(50, 'Nisa', 'Nisa', 'e172dd95f4feb21412a692e73929961e', 'Murobbi', 4, '', 0),
(51, '0', '0', 'd41d8cd98f00b204e9800998ecf8427e', '', 0, '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
