<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('upload_file'))
{
    function upload_file($mode = ''){
    	$CI =& get_instance();
        $config =  array(
                  'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/file_uploaded/",
                  'upload_url'      => base_url()."file_uploaded/",
                  
                  'overwrite'       => TRUE,
                  'max_size'		=> '0'
                );
		
		if ($mode == 'kelompok') {
            $config['allowed_types'] = '*';
            print_r($config['allowed_types']);
        }
		else if ($mode == '') {
            $config['allowed_types'] = "pdf|ppt|pptx";
            print_r($config['allowed_types']);
        }


		//load the upload library
		$CI->load->library('upload', $config);

		$CI->upload->initialize($config);

		//if not successful, set the error message
		if (!$CI->upload->do_upload('file')) {
            echo $CI->upload->display_errors();
			$data['upload_data'] = array('error' => $CI->upload->display_errors());
			return $data['upload_data'];
		} else { //else, set the success message
            echo 'success';
			$data['upload_data'] = $CI->upload->data();
			return $data['upload_data'];
		}		
    }   
}

if ( ! function_exists('is_login')){
	function is_login(){
		$CI =& get_instance();
		return $CI->mysession->is_login();
	}
}


if (! function_exists('count_inbox')){
	function count_inbox(){
		$CI =& get_instance();
		$current_user = $CI->session->userdata('id_user');

		$count = 0;
		$thread = $CI->pesan_model->get_id_thread($current_user);
		foreach ($thread as $t) {
			if (($t['id_penerima'] == $current_user) and ($t['is_delete_penerima'] == 0) and ($t['is_read']==0)){
				$count++;
			} else if (($t['id_pengirim'] == $current_user) and ($t['is_delete_pengirim'] == 0) and ($t['is_read']==0)){
				$count++;
			}
		}
		return $count;
	}
}

if (! function_exists('count_request')){
	function count_request(){
		$CI =& get_instance();
		return $CI->request_model->get_count_pending();
	}
}