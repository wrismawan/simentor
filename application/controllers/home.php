<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kelompok_model');
	}
	public function index()
	{	
		$this->mysession->cek_login();
		$data['count_kelompok_tingkat1'] = $this->kelompok_model->count_kelompok_tingkat1();
		$data['count_kelompok_tingkat2'] = $this->kelompok_model->count_kelompok_tingkat2();
		$data['count_murobbi'] = $this->kelompok_model->count_murobbi();
		$this->load->view('view_home',$data);		
	}

}

/* End of file admin_mentor.php */
/* Location: ./application/controllers/admin_mentor.php */