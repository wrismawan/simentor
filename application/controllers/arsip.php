<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arsip extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('arsip_model');
	}
	public function index()
	{
		$this->mysession->cek_login();
		$tingkat = $_GET['tingkat'];
		$data['arsip'] = $this->arsip_model->get($tingkat);
		$this->load->view('view_arsip',$data);	
	}

}

/* End of file arsip.php */
/* Location: ./application/controllers/arsip.php */