<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('aktivitas_model');
		$this->load->model('kelompok_model');
		$this->load->model('laporan_model');
	}

    private $item_agenda = array('Pembukaan','Tilawah','Kalimat Murabbi','Infaq','Kultum','Mutaba\'ah','Taklimat','Penutup');

	public function index(){
		$this->mysession->cek_login();
		$data['nama_kelompok'] = $_GET['kel'];
		$data['id_murabbi'] = $_GET['m'];
		$data['laporan'] = $this->laporan_model->get_laporan($data['nama_kelompok']);
		$this->load->view('laporan/view_daftar_laporan',$data);
	}

	public function view(){
		$this->mysession->cek_login();

		$data['id_laporan'] = $_GET['lap'];
		$data['nama_kelompok'] = $_GET['kel'];

        $data['id_murabbi'] = $this->session->userdata('id_user');
        $data['binaan'] = $this->kelompok_model->get_binaan_by_kelompok($data['nama_kelompok']);
        $data['aktivitas'] = $this->aktivitas_model->get_aktivitas();

        $data['agenda'] = $this->laporan_model->get_agenda($data['id_laporan']);
        $data['val_agenda'] = explode(',',$data['agenda']['value_agenda']);
        $data['item'] = $this->item_agenda;

        $tmp_mutabaah = $this->laporan_model->get_mutabaah($data['id_laporan']);
        $data['mutabaah'] = $this->reFormatting_mutabaah($tmp_mutabaah);

		$this->load->view('laporan/view_lihat_laporan',$data);
	}

	public function create(){
		$this->mysession->cek_login();
		$data['nama_kelompok'] = $_GET['kel'];
		$data['id_murabbi'] = $this->session->userdata('id_user');
		$data['is_created'] = false;
        $data['item'] = $this->item_agenda;
        //$data['val_agenda'] = array(1,1,1,1,1,1,0,1); //dummy value
		$this->load->view('laporan/view_create_laporan',$data);
	}

	public function evaluasi(){
		$this->mysession->cek_login();
		$data['nama_kelompok'] = $_GET['kel'];
		$data['id_murabbi'] = $this->session->userdata('id_user');
		$data['binaan'] = $this->kelompok_model->get_binaan_by_kelompok($data['nama_kelompok']);
		$data['aktivitas'] = $this->aktivitas_model->get_aktivitas();
		$data['is_created'] = true;
		$id_laporan = $this->laporan_model->get_latest_id($data['nama_kelompok']);
		$data['agenda'] = $this->laporan_model->get_agenda($id_laporan);
        $data['val_agenda'] = explode(',',$data['agenda']['value_agenda']);
        $data['item'] = $this->item_agenda;

        if (isset($_GET['status'])){
            $id_laporan = $_GET['lap'];
            $tmp_mutabaah = $this->laporan_model->get_mutabaah($id_laporan);
            $data['mutabaah'] = $this->reFormatting_mutabaah($tmp_mutabaah);
        }

        $this->load->view('laporan/view_create_laporan',$data);
	}


    public function reFormatting_mutabaah($tmp_mutabaah){
        $binaan = array();
        foreach($tmp_mutabaah as $val){
            if (!in_array($val['id_binaan'],$binaan)){
                //echo ' push - '.$val['id_binaan'].'<br>';
                array_push($binaan,$val['id_binaan']);
                $binaan[$val['id_binaan']] = array();
            }

            $binaan [$val['id_binaan']] [$val['id_aktivitas']] = $val['nilai'];
        }

        return $binaan;
    }

	public function simpan_laporan(){
		$data['id_murabbi'] = $this->input->post('id_murabbi');
		$data['nama_kelompok'] = $this->input->post('nama_kelompok');
		$data['tanggal'] = $this->input->post('tanggal');
        $data['materi'] = $this->input->post('materi');

		$item_agenda = $this->input->post('item_agenda');


        $item = '';
        for ($i = 0; $i<8; $i++){
            $val = isset($item_agenda[$i]) ? 1 : 0;
            $item .= $val.',';
        }
        $data['value_agenda'] = $item;


		$this->laporan_model->simpan_agenda($data);

		$this->session->set_flashdata('message', 'Data Agenda berhasil disimpan');
		redirect("laporan/evaluasi?kel=$data[nama_kelompok]");
	}

//	function simpan_evaluasi_old(){
//		$aktivitas = $this->aktivitas_model->get_aktivitas();
//		$nama_kelompok = $_GET['kel'];
//		$id_murabbi = $_GET['m'];
//
//		$data['id_laporan'] = $this->laporan_model->get_latest_id($nama_kelompok);
//		$data['id_binaan'] = $this->input->post('id_binaan');
//		foreach ($aktivitas as $v) {
//			$data['id_aktivitas'] = $v['id_aktivitas'];
//			if (!empty($this->input->post("act-$v[id_aktivitas]"))){
//				$data['nilai'] = $this->input->post("act-$v[id_aktivitas]");
//			} else {
//				$data['nilai'] = 0;
//			}
//			$this->laporan_model->simpan($data);
//		}
//		redirect("laporan/create?kel=$nama_kelompok&m=$id_murabbi");
//	}

    function simpan_evaluasi(){
        $nama_kelompok = $_GET['kel'];
        $id_laporan = $this->laporan_model->get_latest_id($nama_kelompok);
        $eval = $this->input->post('eval');

        foreach ($eval as $id_binaan => $val_binaan ){
            foreach($val_binaan as $id_aktivitas => $val){
                $data = array(
                    'id_laporan' => $id_laporan,
                    'id_binaan' => $id_binaan,
                    'id_aktivitas' => $id_aktivitas,
                    'nilai' => $val
                );
                $this->laporan_model->simpan_evaluasi($data);
            }
        }

        $this->session->set_flashdata('message','Data berhasil disimpan');
        redirect("laporan/evaluasi?kel=$nama_kelompok&status=success&lap=$id_laporan");
    }

	function ajax_evaluasi(){
		$html = '<thead><th>Aktivitas</th><th>Nilai</th></thead>';
                                   
		$id_laporan = $_GET['id_laporan'];
		$id_binaan = $_GET['id_binaan'];

		$evaluasi = $this->laporan_model->get_evaluasi($id_laporan, $id_binaan);

		if ($evaluasi){
			foreach ($evaluasi as $v) {
				$html .= "
				<tr>
					<td>$v[nama_aktivitas]</td>
					<td>
						 <input id=\"act-$v[id_aktivitas]\" type=\"number\" name=\"act-$v[id_aktivitas]\" value=\"$v[nilai]\"/>
					</td>
				</tr>";
			}
		} else {
			$html .= 'Belum mengisi evaluasi';
		}
		echo $html;
	}


}

/* End of file laporan.php */				
/* Location: ./application/controllers/laporan.php */