<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('pesan_model');
	}

	public function index(){
		$this->mysession->cek_login();
		$inbox = array();
		$current_user  = $this->session->userdata('id_user');
		$tmp_pesan = $this->pesan_model->get_inbox($current_user);
		
		foreach ($tmp_pesan as $pesan) {
			if (($current_user == $pesan['id_pengirim']) and ($pesan['is_delete_pengirim'] == 0)){
				array_push($inbox, $pesan);
			} 
			else if (($current_user == $pesan['id_penerima']) and ($pesan['is_delete_penerima'] == 0)){
				array_push($inbox, $pesan);
			}
		}

		$data['penerima'] = $this->user_model->get_penerima_pesan($current_user);
		$data['id_user'] = $current_user;
		$data['pesan'] = $inbox;

		$this->load->view('pesan/view_pesan',$data);
		
	}

	public function view(){
		$this->mysession->cek_login();
		
		$data['id_thread'] = $_GET['id'];
		$data['id_penerima'] = $_GET['rcv'];
		$data['id_pengirim'] = $_GET['snd'];

		if ($this->is_owner($data)){
			$data['thread'] = $this->pesan_model->get_thread($data['id_thread']);
			
			if (!isset($_GET['status']))
				$this->pesan_model->status_read($data['id_thread'], 1);

			$this->load->view('pesan/view_lihat_pesan',$data);
		}
		
	}

	private function is_owner($data){
		$owner = $this->pesan_model->get_owner($data['id_thread']);

		if (($owner['id_penerima']==$data['id_penerima']) && ($owner['id_pengirim']==$data['id_pengirim']))
			return true;
		else 
			return false;	
	}


	public function reply(){
		$current_user = $this->session->userdata('id_user');
		$receiver = $_GET['rcv']; 
		$sender = $_GET['snd'];

		$data['id_pengirim'] = $current_user;

		if ($current_user == $receiver)
			$data['id_penerima'] = $sender;
		else 
			$data['id_penerima'] = $receiver;

		$data['id_thread'] = $this->input->post('id_thread');
		$data['isi_pesan'] = $this->input->post('isi_pesan');

		$this->pesan_model->simpan($data);
		$this->session->set_flashdata('message', 'Pesan terkirim');
		$this->pesan_model->status_read($data['id_thread'],0);
		$this->pesan_model->reset_delete($data['id_thread']);

		redirect("pesan/view?id=$data[id_thread]&rcv=$receiver&snd=$sender&status=success");
	}

	public function send(){
		$data['id_thread'] = $this->pesan_model->get_latest_thread() + 1;		
		$data['judul_pesan'] = $this->input->post('judul_pesan');
		$data['id_penerima'] = $this->input->post('penerima');
		$data['id_pengirim'] = $this->input->post('pengirim');

		$data['isi_pesan'] = $this->input->post('isi_pesan');
		$this->pesan_model->simpan($data);
		$this->session->set_flashdata('message', 'Pesan terkirim');
		redirect('pesan');
	}

	function delete(){
		$data['id_user'] = $this->session->userdata('id_user');
		$data['id_thread'] = $_GET['id'];

		$owner = $this->pesan_model->get_owner($data['id_thread']);

		if ($data['id_user'] == $owner['id_penerima'])
			$data['mode'] = 'penerima';
		else
			$data['mode'] = 'pengirim';

		$this->pesan_model->delete($data);

		$this->session->set_flashdata('message', 'Pesan berhasil dihapus');
		redirect('pesan');
	}

}

/* End of file pesan.php */
/* Location: ./application/controllers/pesan.php */