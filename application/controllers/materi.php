<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materi extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('simentor_helper');
		$this->load->model('materi_model');
		
	}

	public function index()
	{
		$this->mysession->cek_login();

		$role = $this->session->userdata('role');

		if (($role == 'Admin Murobbi') or ($role == 'Murobbi'))
			$tingkatan = 2;
		else
			$tingkatan = 1;
		$data['materi'] = $this->materi_model->get_all($tingkatan); #tingkatan
		$this->load->view('view_materi',$data);
	}

	function upload() {

		$nama_materi = $this->input->post('nama_materi');
		$upload_data = upload_file();

		$role = $this->session->userdata('role');
		if (($role == 'Admin Murobbi') or ($role == 'Murobbi'))
			$tingkatan = 2;
		else
			$tingkatan = 1;

		if ($upload_data!=NULL){
			$data = array(
				'path' => 'file_uploaded/'.$upload_data['orig_name'],
				'tingkatan' => $tingkatan,
				'nama_materi' => $nama_materi
			);
			
			$this->load->model('materi_model');
			$this->materi_model->simpan($data);

			$msg = "Alhamdulillah. File $upload_data[orig_name] berhasil di-upload.";
		} else {
			$msg = "Alhamdulillah. File $upload_data[orig_name] berhasil di-upload.";
		}

		$this->session->set_flashdata('message', $msg);
		redirect('materi');
	}

	function delete($id_materi){
		$del = $this->materi_model->delete($id_materi);
		if ($del){
			$msg = 'Materi berhasil dihapus';
		} else {
			$msg = 'Materi gagal dihapus';
		}
		$this->session->set_flashdata('message', $msg);
		redirect('materi');

	}



}

/* End of file materi.php */
/* Location: ./application/controllers/materi.php */