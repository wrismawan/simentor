<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('fakultas_model');
	}
	public function index(){
		$this->mysession->cek_login();
		$data['user'] = $this->user_model->get_all_user();
		$data['fakultas'] = $this->fakultas_model->get_all();
		$this->load->view('user/view_daftar_user', $data);
	}

	public function create(){
		$this->mysession->cek_login();
		$this->load->view('user/view_create_user');
	}

	public function edit($id_user){
		$this->mysession->cek_login();
		$data['user'] = $this->user_model->get_user($id_user);
		$data['fakultas'] = $this->fakultas_model->get_all();
		$data['jabatan'] = array('Admin Mentor'=>1,'Mentor'=>2,'Admin Murobbi'=>3,'Murobbi'=>4);
		$this->load->view('user/view_edit_user',$data);
	}

	public function simpan(){
		$data['nama'] = $this->input->post('nama');
		$data['username'] = $this->input->post('username');
		$data['password'] = md5($this->input->post('password'));
		$data['role'] = $this->input->post('role');
		$data['id_fakultas'] = $this->input->post('fakultas');

		if (!$this->is_exist($data['username'])){	
			$simpan = $this->user_model->simpan($data);
			if ($simpan){
				$msg = "User baru barhasil dibuat";
			} else {
				$msg = "Gagal membuat user baru";
			}
			$this->session->set_flashdata('message', $msg);
			redirect('user');
		} else {
			$msg = "Username sudah ada sebelumnya";
			
			$data['is_edit'] = true;
			$data['user'] = $this->user_model->get_all_user();
			$data['fakultas'] = $this->fakultas_model->get_all();

			$this->session->set_flashdata('message', $msg);
			$this->load->view('user/view_daftar_user', $data);
		}


	}

	function is_exist($username){
		$get = $this->user_model->get_username($username);
		if ($get)
			return true;
		else
			return false;
	}

	public function update(){
		$data['nama'] = $this->input->post('nama');
		$data['username'] = $this->input->post('username');
		$data['password'] = md5($this->input->post('password'));
		$data['role'] = $this->input->post('role');
		$data['id_fakultas'] = $this->input->post('fakultas');
		$data['id_user'] = $this->input->post('id_user');

		$this->user_model->update($data);
		$this->session->set_flashdata('message', "Data user berhasil diperbaharui");
		redirect('user');

	}

	function delete($id_user){
		$delete = $this->user_model->delete($id_user);
		redirect('user');
	}

	function change_password(){
		$id_user = $_GET['id'];
		$old_password = md5($this->input->post('old_password'));
		$new_password = $this->input->post('new_password');
		$password = $this->user_model->get_password($id_user);
		
		if ($old_password == $password){
			$this->user_model->save_new_password($id_user, md5($new_password));
			$message = 'Password berhasil diubah';
		} else {
			$message = 'Password lama tidak sama dengan yang dimasukkan. Silahkan coba lagi';
		}

		$this->session->set_flashdata('message', $message);
		redirect("user/edit/$id_user");
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */