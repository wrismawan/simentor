<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('request_model');
		$this->load->model('kelompok_model');
		$this->load->model('arsip_model');
	}

	function index()
	{
		$this->mysession->cek_login();
		$data['request'] = $this->request_model->get_request();
		$this->load->view('view_request', $data);
	}

	function approve(){
		$this->mysession->cek_login();

		$id_request = $_GET['req'];
		$id_binaan = $_GET['bin'];

		$data_binaan = $this->kelompok_model->get_binaan($id_binaan);
		$this->arsip_model->simpan_binaan($data_binaan);
		$this->kelompok_model->delete_binaan($id_binaan);
		$update = $this->request_model->approve_request($id_request);


		if ($update){
			$message = "Approve berhasil";
		} else {
			$message = "Approve gagal";
		}
		
		$this->session->set_flashdata('message', $message);
		
		redirect("request");
	}

}

/* End of file request.php */
/* Location: ./application/controllers/request.php */