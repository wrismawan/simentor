<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Kelompok extends CI_Controller
{

    public function __construct()

    {

        parent::__construct();

        $this->load->helper('simentor_helper');
        $this->load->library('CSVReader');
        $this->load->model('kelompok_model');
        $this->load->model('user_model');
    }


    public function view()
    {

        $this->mysession->cek_login();
        $id_murabbi = $this->session->userdata('id_user');
        $role = $this->session->userdata('role');

        if (isset($_GET['tingkat']) and (($role == 'Admin Murobbi') or ($role == 'Admin Mentor'))) {

            $tingkat = $_GET['tingkat'];

            $data['kelompok'] = $this->kelompok_model->get_daftar_kelompok_for_admin($tingkat);

            $data['title'] = "Daftar Kelompok Tingkat $tingkat";

        } else {

            $data['kelompok'] = $this->kelompok_model->get_daftar_kelompok($id_murabbi);

            $data['title'] = "Daftar Kelompok Saya";

        }


        $this->load->view('kelompok/view_lihat_kelompok', $data);

    }


    public function detail()
    {

        $nama_kelompok = $_GET['kel'];

        $this->mysession->cek_login();


        $data['kelompok'] = $this->kelompok_model->get_detail_kelompok($nama_kelompok);

        $this->load->view('kelompok/view_detail_kelompok', $data);

    }


    public function kelola()
    {

        $this->mysession->cek_login();

        $this->load->view('kelompok/view_kelola_kelompok');

    }


    public function submit_kelompok()
    {

        $role = $this->session->userdata('role');

        if ($role == 'Admin Murobbi') $tingkatan = 2; else $tingkatan = 1;


        $upload_data = upload_file('kelompok');

        $kelompok = $this->csvreader->parse_file($upload_data['full_path']);


        #get nama user from database

        $user = $this->user_model->get_nama();

        $list_nama = array();

        foreach ($user as $value) {

            array_push($list_nama, $value['Nama']);

        }


        $new_user = array();

        $id_binaan = intval($this->kelompok_model->get_latest_id_binaan()) + 1;

        foreach ($kelompok as $kel) {

            if (!in_array($kel['nama_murabbi'], $list_nama, true)) {

                $new_user['nama'] = $kel['nama_murabbi'];

                $new_user['username'] = $this->generate_username($kel['nama_murabbi']);

                $new_user['password'] = md5('bismillah');

                if ($tingkatan == 1)

                    $new_user['role'] = 2;

                else

                    $new_user['role'] = 4;

                array_push($list_nama, $new_user['nama']);

                $this->user_model->simpan($new_user);


                $kel['tingkatan'] = $tingkatan;

                $kel['id_murabbi'] = $this->user_model->get_id_user($new_user['username'], $new_user['nama']);


                $kel['id_binaan'] = $id_binaan;

                echo $id_binaan++;


                #var_dump($kel); echo '<br><br>';


                $this->kelompok_model->simpan($kel);

            } else {

                $kel['id_murabbi'] = $this->user_model->get_id_user('', $kel['nama_murabbi']);

                $kel['id_binaan'] = $id_binaan;

                echo $id_binaan++;

                $kel['tingkatan'] = $tingkatan;

                #var_dump($kel); echo '<br><br>';


                $this->kelompok_model->simpan($kel);

            }


        }


        if (isset($upload_data['error']))

            $message = 'Error : File yang di-upload harus dalam format CSV';

        else

            $message = 'Data kelompok berhasil disimpan';


        $this->session->set_flashdata('message', $message);

        //redirect('kelompok/kelola');

    }


    function generate_username($nama)
    {

        $parse = explode(' ', $nama);

        $username = $parse[0] . rand(1, 99);

        return $username;

    }


    function delete()
    {

        $nama_kelompok = $_GET['kel'];

        $this->kelompok_model->delete_kelompok($nama_kelompok);

        $this->session->set_flashdata('message', 'Kelompok berhasil dihapus');

        redirect('kelompok/view');

    }


}



/* End of file halaqah.php */

/* Location: ./application/controllers/halaqah.php */