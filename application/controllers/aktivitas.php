<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktivitas extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('aktivitas_model');
	}
	public function kelola(){		
		$this->mysession->cek_login();
		$data['aktivitas'] = $this->aktivitas_model->get_aktivitas();
        $this->load->view('view_kelola_aktivitas',$data);
	}

	public function add(){
		$data['nama_aktivitas'] = $this->input->post('nama_aktivitas');
		$this->aktivitas_model->simpan($data);

		$this->session->set_flashdata('message', 'Berhasil ditambahkan');

		redirect('aktivitas/kelola');
	}

	function delete($id_aktivitas){
		$this->aktivitas_model->delete($id_aktivitas);
		redirect('aktivitas/kelola');
	}

}

/* End of file aktivitas.php */
/* Location: ./application/controllers/aktivitas.php */