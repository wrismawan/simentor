<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	function login(){
		if($this->session->userdata('login')){
			redirect('home');
		}else{
			$this->load->view('view_login');
		}
	}

	function login_process(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$userdata = $this->user_model->get_userdata($username, $password);
		#var_dump($userdata);
		if ($userdata){
			$data = array(
				'login'			=> true,
				'id_user' 		=> $userdata['id_user'],
				'username'	 	=> $userdata['username'],
				'role'	 		=> $userdata['role'],
				'id_fakultas' 	=> $userdata['id_fakultas']
			);
			$this->session->set_userdata($data);
			redirect('home');
		} else {
			$this->session->set_flashdata('message', 'Username atau Password Anda salah');
			redirect('auth/login');
		}
	}

	function logout(){
		$this->session->unset_userdata();
		$this->session->sess_destroy();
		$this->session->set_userdata('login',false);
		redirect('auth/login');
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */