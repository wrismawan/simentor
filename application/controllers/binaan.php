<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Binaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kelompok_model');
		$this->load->model('arsip_model');
	}
	public function edit($id_binaan)
	{
		$this->mysession->cek_login();
		$data['binaan'] = $this->kelompok_model->get_binaan($id_binaan);
		$this->load->view('binaan/view_edit_binaan',$data);
	}

	public function delete(){
		$this->mysession->cek_login();
		$id_binaan = $_GET['bin'];
		$kelompok = $_GET['kel'];

		$this->kelompok_model->delete_binaan($id_binaan);

		$this->session->set_flashdata('message', 'Binaan berhasil dihapus');
		redirect("kelompok/detail?kel=$kelompok");
	}

	function simpan(){
		$data['id_binaan'] = $this->input->post('id_binaan');
		$data['nama_binaan'] = $this->input->post('nama');
		$data['fakultas_binaan'] = $this->input->post('fakultas');
		$data['jurusan_binaan'] = $this->input->post('jurusan');
		$data['angkatan_binaan'] = $this->input->post('angkatan');
		$data['no_hp_binaan'] = $this->input->post('telp');

		$x = $this->kelompok_model->update($data);

		redirect("binaan/edit/$data[id_binaan]");
	}

	function level_up($id_binaan){
		$this->kelompok_model->update_promote($id_binaan,0);
		return $this->kelompok_model->level_up($id_binaan);
	}


	function request_up($id_binaan){
		$tingkatan = $this->kelompok_model->get_tingkatan($id_binaan);
		
		if ($tingkatan == 2){ #tingkat 2 tidak perlu approval dari admin
			$data_binaan = $this->kelompok_model->get_binaan($id_binaan);
			$this->arsip_model->simpan_binaan($data_binaan);
			$this->kelompok_model->delete_binaan($id_binaan);
			$message = 'Naik tingkat berhasil';
		} else { 
			$data['id_binaan'] = $id_binaan;
			$data['tingkatan'] = $tingkatan;
			$this->kelompok_model->update_promote($id_binaan,1);
			$simpan = $this->request_model->simpan_request($data);
			if ($simpan)
				$message = "Request naik tingkat berhasil, silahkan tunggu persetujuan dari Admin";
			else 
				$message = "Request naik tingkat gagal. Silahkan coba lagi";
		}	

		$this->session->set_flashdata('message', $message);
		$nama_kelompok = implode("", $this->kelompok_model->get_nama_kelompok($id_binaan));	
		redirect("kelompok/detail?kel=$nama_kelompok");

	}
}

	/* End of file binaan.php */
/* Location: ./application/controllers/binaan.php */