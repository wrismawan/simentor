<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="active">
                <a href="<?php echo base_url(); ?>">
                    <i class="fa fa-home"></i><span>Home</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('pesan'); ?>">
                    <i class="fa fa-envelope"></i><span>Pesan</span>
                    <?php $count =  count_inbox(); if ($count > 0) : ?>
                    <small class="badge pull-right bg-yellow"><?=$count?></small>
                <?php endif ?>
            </a>
        </li>
        
        <?php if ($this->session->userdata('role') == 'Admin Murobbi') : ?>
        <li>
            <a href="<?php echo base_url('request'); ?>">
                <i class="fa fa-bell-o"></i> <span>Request</span>
                <?php 
                $count_request = count_request();
                if ($count_request > 0) { ?>
                <small class="badge pull-right bg-red"><?php echo $count_request ?></small>
                <?php } ?>
            </a>
        </li>
    <?php endif ?>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-group"></i>
            <span>Kelompok</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <?php 
            $role = $this->session->userdata('role'); 
            if (( $role == 'Admin Murobbi') or ($role =='Admin Mentor')): ?>
            <li><a href="<?php echo base_url('kelompok/kelola'); ?>"><i class="fa fa-angle-double-right"></i> Buat Kelompok</a></li>
        <?php endif ?>
        <li><a href="<?php echo base_url('kelompok/view'); ?>"><i class="fa fa-angle-double-right"></i> Kelompok Saya</a></li>

        <?php if ( $role == 'Admin Murobbi') :?>
        <li><a href="<?php echo base_url('kelompok/view?tingkat=1'); ?>"><i class="fa fa-angle-double-right"></i> Kelompok Tingkat 1</a></li>
        <li><a href="<?php echo base_url('kelompok/view?tingkat=2'); ?>"><i class="fa fa-angle-double-right"></i> Kelompok Tingkat 2</a></li>
    <?php endif ?>

    <?php if ( $role == 'Admin Mentor') :?>
    <li><a href="<?php echo base_url('kelompok/view?tingkat=1'); ?>"><i class="fa fa-angle-double-right"></i> Kelompok Tingkat 1</a></li>
<?php endif ?>
</ul>
</li>   
<li>
    <a href="<?php echo base_url('materi'); ?>">
        <i class="fa fa-book"></i>
        <span>Materi</span>
    </a>
</li>
<?php $role = $this->session->userdata('role');
if (($role == 'Admin Murobbi') or ($role == 'Admin Mentor')) : ?>
<li class="treeview">
    <a href="<?php echo base_url('arsip'); ?>">
        <i class="fa fa-folder-open-o"></i> <span>Arsip</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="<?php echo base_url('arsip?tingkat=1'); ?>"><i class="fa fa-angle-double-right"></i> Tingkat 1</a></li>
        <?php if ($role == 'Admin Murobbi') : ?>
        <li><a href="<?php echo base_url('arsip?tingkat=2'); ?>"><i class="fa fa-angle-double-right"></i> Tingkat 2</a></li>
    <?php endif ?>
</ul>
</li>
<?php endif ?>

<?php if ($this->session->userdata('role') == 'Admin Murobbi'){ ?>
<li class="treeview">
    <a href="<?php echo base_url('user'); ?>">
        <i class="fa fa-gears"></i> <span>Setting</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="<?php echo base_url('user'); ?>"><i class="fa fa-angle-double-right"></i> User</a></li>
        <li><a href="<?php echo base_url('aktivitas/kelola'); ?>"><i class="fa fa-angle-double-right"></i> Aktivitas Ibadah</a></li>
    </ul>
</li>
<?php } ?>
</ul>
</section>
<!-- /.sidebar -->
</aside>