<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">

    <?php $this->load->view('header'); ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <?php $this->load->view('sidebar'); ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Laporan
                    <small>Daftar Laporan</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
                    <li class="active">Daftar</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Edit Profile</h3>  
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div><!-- /. tools -->                                  
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <!-- form -->
                                        <?php echo form_open('binaan/simpan'); ?>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="hidden" name="id_binaan" value ="<?php echo $binaan['id_binaan']; ?>">
                                                <input class="form-control" type="text"
                                                 placeholder="Nama" name="nama" value ="<?php echo $binaan['nama_binaan']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Fakultas</label>
                                                <input class="form-control" type="text" placeholder="Fakultas" name="fakultas" value ="<?php echo $binaan['fakultas_binaan']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Jurusan</label>
                                                <input class="form-control" type="text" placeholder="Jurusan" name="jurusan" value ="<?php echo $binaan['jurusan_binaan']; ?>">
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label>Angkatan</label>
                                                <input class="form-control" type="text" placeholder="Angkatan" name="angkatan" value ="<?php echo $binaan['angkatan_binaan']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>No Telepon</label>
                                                <input class="form-control" type="text" placeholder="No Telp" name="telp" value ="<?php echo $binaan['no_hp_binaan']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- /.row -->
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div><!-- /.box -->
                    </div> <!-- /.col-md-12 -->
                </div>

            </section>
            <!-- /.Main content  -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->

    <!-- add new calendar event modal -->

    <?php $this->load->view('script');?>
</body>
</html>