<?php include 'head.php'; ?>

<body class="bg-black">
    <?php include 'notif.php'; ?>
    <div class="form-box" id="login-box">
        <div class="header">Sign In</div>
        <?php echo form_open('auth/login_process'); ?>
            <div class="body bg-gray">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username"/>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                </div>
            </div>
            <div class="footer">                                                               
                <button type="submit" class="btn bg-olive btn-block">Sign me in</button>  
            </div>
        </form>

        
    </div>


    <!-- jQuery 2.0.2 -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>        

</body>