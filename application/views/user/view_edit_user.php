<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Laporan
                <small>Daftar Laporan</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
                <li class="active">Daftar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Edit Profile</h3>

                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip"
                                        title="Collapse"><i class="fa fa-minus"></i></button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">

                                <!-- form -->
                                <?php echo form_open('user/update'); ?>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="hidden" name="id_user" value="<?php echo $user['id_user']; ?>">
                                        <input class="form-control" type="text"
                                               name="nama" value="<?php echo $user['Nama']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="form-control" type="text" name="username"
                                               value="<?php echo $user['username']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Fakultas</label>
                                        <select class="form-control" name="fakultas">
                                            <?php foreach ($fakultas as $value) {
                                                if ($value['id_fakultas'] == $user['id_fakultas']) {
                                                    ?>
                                                    <option value="<?php echo $value['id_fakultas']; ?>"
                                                            selected="selected"><?php echo $value['nama']; ?></option>
                                                <?php } else { ?>
                                                    <option
                                                        value="<?php echo $value['id_fakultas']; ?>"><?php echo $value['nama']; ?></option>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Role</label>
                                        <select class="form-control" name="role">
                                            <?php foreach ($jabatan as $var => $val) : ?>
                                                <option value="<?php echo $val ?>"
                                                        <?php if ($var == $user['role']): ?>selected="selected"<?php endif; ?>>
                                                    <?php echo $var; ?>
                                                </option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!-- /.row -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col-md-12 -->
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Edit Password</h3>

                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip"
                                        title="Collapse"><i class="fa fa-minus"></i></button>
                            </div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <?php echo form_open("user/change_password?id=$user[id_user]"); ?>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Password Lama</label>
                                        <input class="form-control" type="password" name="old_password">
                                    </div>
                                    <div class="form-group">
                                        <label>Password Baru</label>
                                        <input class="form-control" type="password" name="new_password">
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>
        <!-- /.Main content  -->

    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script'); ?>
</body>
</html>