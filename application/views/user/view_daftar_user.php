<!DOCTYPE html>
<html>

<script type="text/javascript">
    <?php if (isset($is_edit)) : ?>
    alert('username sudah digunakan sebelumnya');
    <?php endif ?>
</script>

<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header no-margin">
            <h1 class="text-center">
                User
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- MAILBOX BEGIN -->
            <div class="mailbox row">
                <div class="col-xs-12">
                    <div class="box-body">
                        <div class="form-group">
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#user-modal">Add
                                User
                            </button>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="table-daftar-user" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;
                            foreach ($user as $value) { ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value['Nama']; ?></td>
                                    <td><?php echo $value['username']; ?></td>
                                    <td><?php echo $value['role']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url("user/edit/$value[id_user]"); ?>"><i
                                                class="fa fa-edit"></i></a>&nbsp&nbsp
                                        <a href="<?php echo base_url("user/delete/$value[id_user]"); ?>"
                                           onclick="return confirm('Apakah Anda yakin akan menghapus user ini?')"><i
                                                class="fa fa-trash-o"></i></a>&nbsp&nbsp
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- MAILBOX END -->

                <!-- COMPOSE MESSAGE MODAL -->
                <div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Add User </h4>
                            </div>

                            <div class="modal-body">
                                <?php echo form_open('user/simpan'); ?>
                                <div class="form-group">
                                    <label for="nama_materi">Nama Lengkap</label>
                                    <input type="text" class="form-control" name="nama"
                                           placeholder="Masukkan Nama Lengkap"
                                           value="<?php if (isset($is_edit)) echo $nama; ?>" required>
                                </div>
                                <div class="form-group">
                                    <label>Fakultas</label>
                                    <select class="form-control" name="fakultas" required
                                            value="<?php if (isset($is_edit)) echo $fakultas; ?>">
                                        <?php foreach ($fakultas as $value) { ?>
                                            <option
                                                value="<?php echo $value['id_fakultas']; ?>"><?php echo $value['nama']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nama_materi">Username</label>
                                    <input type="text" class="form-control" name="username"
                                           placeholder="Masukkan Username" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama_materi">Password</label>
                                    <input type="password" class="form-control" name="password"
                                           placeholder="Masukkan Password" required>
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" name="role" required
                                            value="<?php if (isset($is_edit)) echo $role; ?>">
                                        <option value="1">Admin Mentor</option>
                                        <option value="2">Mentor</option>
                                        <option value="3">Admin Murobbi/ah</option>
                                        <option value="4">Murobbi/ah</option>
                                    </select>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                </form>
                            </div>

                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->

    </section>
    <!-- /.Main content  -->

    </aside><!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script'); ?>
</body>
</html>