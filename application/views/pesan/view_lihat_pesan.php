<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header no-margin">
                <h1>
                    Judul : <?php echo $thread[0]['judul_pesan']; ?>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">               
                <div class="slimScrollDiv" style="position: relative; overflow;">
                    <div class="box-body chat" id="chat-box" style="overflow">

                        <?php foreach ($thread as $val) { ?>
                        <div class="item" style="border-bottom:1px solid #e5e5e5">
                            <p class="message">
                                <a href="#" class="name">
                                    <small class="text-muted pull-right">
                                        <i class="fa fa-clock-o"></i> <?php echo $val['tanggal']; ?>
                                    </small>
                                    <b><?php echo $val['nama']; ?></b>
                                </a>
                                <br>
                                <?php echo $val['isi_pesan']; ?>
                            </p>
                        </div>
                        <?php } ?>
                        <br>
                        <?php echo form_open("pesan/reply?rcv=$id_penerima&snd=$id_pengirim"); ?>
                        <div class="form-group">
                            <input type="hidden" name="id_thread" value="<?php echo $id_thread; ?>">
                            <textarea name="isi_pesan" class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                        <div class="form-footer clearfix">
                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>              
                
                <!-- MAILBOX END -->
            </section><!-- /.content -->
        </aside><!-- /.right-side -->

    </section>
    <!-- /.Main content  -->

</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script'); ?>
</body>
</html>