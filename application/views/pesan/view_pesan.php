<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header no-margin">
                <h1 class="text-center">
                    Mailbox
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- MAILBOX BEGIN -->
                <div class="mailbox row">
                    <div class="col-xs-12">
                        <div class="box-body">
                            <div class="form-group">
                                <button class= "btn btn-primary btn-sm" data-toggle="modal" data-target="#compose-modal">COMPOSE</button>
                            </div>
                        </div>
                        <div class="box-body table-responsive">
                            <table id="table-kelompok" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Pengirim</th>
                                        <th>Judul</th>
                                        <th>Tanggal</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($pesan as $value) { 
                                        if ($value['is_read']==0)
                                            echo "<tr class=\"bold\">";
                                        else 
                                            echo "<tr>";
                                        ?>

                                        <td><?php echo $value['nama']; ?></td>
                                        <td><?php echo $value['judul_pesan']; ?></td>
                                        <td><?php echo $value['tanggal']; ?></td>
                                        <td>    
                                            <a href="<?php echo base_url("pesan/view?id=$value[id_thread]&rcv=$value[id_penerima]&snd=$value[id_pengirim]"); ?>"><i class="fa fa-eye"></i></a>&nbsp&nbsp
                                            <a href="<?php echo base_url("pesan/delete?id=$value[id_thread]"); ?>" onclick="return confirm('Apakah Anda yakin akan menghapus pesan ini?');"><i class="fa fa-trash-o"/>&nbsp&nbsp
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div>
                    <!-- MAILBOX END -->

                    <!-- COMPOSE MESSAGE MODAL -->
                    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
                                </div>
                                <?php echo form_open('pesan/send/submit'); ?>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="hidden" name="pengirim" value="<?php echo $this->session->userdata('id_user'); ?>">
                                            <span class="input-group-addon">TO:</span>
                                            <select name="penerima" class="form-control">
                                                <?php foreach ($penerima as $value) {
                                                    echo "<option id=\"$value[id_user]\" value=\"$value[id_user]\">$value[Nama] - $value[role]</option>";
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="text" name="judul_pesan" class="form-control" placeholder="Subject"><br>
                                    </div>

                                    <div class="form-group">
                                        <textarea class="textarea" name="isi_pesan" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                    
                                </div>
                                <div class="modal-footer clearfix">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>
                                    <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
                                </div>
                                <?php echo form_close(); ?>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

        </section>
        <!-- /.Main content  -->

    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script'); ?>
</body>
</html>