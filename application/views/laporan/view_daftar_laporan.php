<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Laporan
                    <small>Daftar Laporan</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
                    <li class="active">Daftar</li>
                </ol>
            </section>
           
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Daftar Laporan</h3>  
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                </div><!-- /. tools -->                                  
                            </div><!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <div class="row">
                                    <div class="from-group col-md-2">
                                        <a href="<?php echo base_url("laporan/create?kel=$_GET[kel]&m=$_GET[m]"); ?>"><button class="btn btn-primary">Buat Laporan</button></a>
                                    </div>
                                </div>
                                <br>
                                <table id="table-kelompok" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kelompok</th>
                                            <th>Materi</th>
                                            <th>Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0; foreach ($laporan as $value): ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value['nama_kelompok']?></td>
                                            <td><?=$value['materi']?></td>
                                            <td><?=$value['tanggal']?></td>
                                            <td>
                                                <a href="<?php echo base_url("laporan/view?kel=$value[nama_kelompok]&m=$_GET[m]&lap=$value[id_laporan]"); ?>"><i class="fa fa-eye"/></i></a>&nbsp&nbsp
                                                <i class="fa fa-edit"/>&nbsp&nbsp
                                                <i class="fa fa-trash-o"/>&nbsp&nbsp
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div> <!-- /.col-md-12 -->

            </div>

        </section>
        <!-- /.Main content  -->

    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script'); ?>

</body>
</html>