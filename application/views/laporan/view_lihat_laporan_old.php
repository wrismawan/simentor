<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Laporan
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Agenda</h3>
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <!-- tools box -->

                        <!-- form start -->

                        <div class="box-body">
                            <?php echo form_open('laporan/simpan_laporan'); ?>
                            <div class="row">
                                <input type="hidden" name="nama_kelompok" value="<?php echo $agenda['nama_kelompok']; ?>">
                                <input type="hidden" name="id_murabbi" value="<?php echo $agenda['id_murabbi']; ?>">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            Tanggal
                                        </label>
                                        <input type="text" name="tanggal" id="tgl-laporan" value="<?php echo $agenda['tanggal']; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Materi
                                        </label>
                                        <input type="text" name="materi" value="<?php echo $agenda['materi']; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="checkbox">
                                        
                                        <label><input type="checkbox" name="pembukaan" value="1" <?php $checked = 'checked'; if ($agenda['pembukaan']) echo $checked; ?>/> Pembukaan</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="tilawah" value="1" <?php if ($agenda['tilawah']) echo $checked; ?>/> Tilawah</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="kalimat" value="1" <?php if ($agenda['kalimat_MR']) echo $checked; ?>/> Kalimat Murabbi</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="infaq" value="1" <?php if ($agenda['infaq']) echo $checked; ?>/> Infaq</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="kultum" value="1" <?php if ($agenda['kultum']) echo $checked; ?>/> Kultum</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="mutaba_ah" value="1" <?php if ($agenda['mutaba_ah']) echo $checked; ?>/> Mutaba'ah</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="taklimat" value="1" <?php if ($agenda['taklimat']) echo $checked; ?>/> Taklimat</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="penutup" value="1" <?php if ($agenda['penutup']) echo $checked; ?>/> Penutup</label>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <?php echo form_close(); ?>
                        </div>
                    </div><!-- /.box -->
                </div>

                <!-- LAPORAN MODAL -->
                <div class="modal fade" id="laporan-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Evaluasi Ibadah</h4>
                            </div>
                            <!-- <form action="#" method="post"> -->
                            <div class="modal-body">
                                <?php echo form_open("laporan/simpan_evaluasi?kel=$_GET[kel]&m=$_GET[m]"); ?>
                                <input type="hidden" id="id_binaan" name="id_binaan"/>
                                
                                <table id="table-evaluasi-val" class="table table-bordered table-striped">
                                </table>

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END OF LAPORAN MODAL -->
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Daftar Binaan</h3>  
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->                                  
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="table-laporan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama Binaan</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($binaan as $key => $value) { ?>
                                    <tr>
                                        <td><?=$value['nama_binaan']?></td>
                                        <td>Sudah</td>
                                        <td><a class="btn-eval" id="bin-<?=$value['id_binaan']?>" href="#" data-toggle="modal" data-target="#laporan-modal"><i class="fa fa-edit"></i></a>&nbsp&nbsp</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div> <!-- /.col-md-12 -->
            </div>
        </section>
        <!-- /.Main content  -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<?php $this->load->view('script'); ?>
<script type="text/javascript">

$(".btn-laporan").click(function(e){
    var id_binaan = this.id;
    $("#id_binaan").val(id_binaan);
})

$(".btn-eval").on('click', function(){
    var id_binaan = this.id.split('bin-')[1];
    var id_laporan = <?php echo $_GET['lap']; ?>

    console.log(id_binaan+'-'+id_laporan);
    $.ajax({
      type: "GET",
      url: "<?php echo base_url('laporan/ajax_evaluasi'); ?>",
      data: "id_binaan="+id_binaan+"&id_laporan="+id_laporan,
      cache: false,
      success: function(html){
        $("#table-evaluasi-val").append(html);
      }, 
      error: function(e){
        console.log(e);
      }
    })
  })
</script>
</body>
</html>