<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">
<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Laporan
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="col-md-12">
                <div class='box box-primary'>
                    <div class="box-header">
                        <h3 class="box-title">Agenda</h3>

                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip"
                                    title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <!-- tools box -->

                    <!-- form start -->

                    <div class="box-body" >
                        <?php echo form_open('laporan/simpan_laporan'); ?>
                        <div class="row">
                            <input type="hidden" name="nama_kelompok" value="<?php echo $nama_kelompok; ?>">
                            <input type="hidden" name="id_murabbi" value="<?php echo $id_murabbi; ?>">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <!-- <input class="form-control" type="text" name="tanggal" id="tgl-create" placeholder="Tanggal Mentoring"/> -->
                                    <div class='input-group date'>
                                        <input type='text' id="tgl-create-laporan" class="form-control" name="tanggal"
                                               placeholder="Tanggal Mentoring" value="<?= $agenda['tanggal'] ?>" />
                                        <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Materi</label>
                                    <input class="form-control" type="text" name="materi"
                                           placeholder="Judul Materi Mentoring" value="<?= $agenda['materi'] ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <?php

                                for ($i = 0; $i < 4; $i++) : ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="item_agenda[<?= $i ?>]"
                                                <?php if ($val_agenda[$i] == '1') : ?> value="1" checked <?php endif ?> />
                                            <?= $item[$i] ?>
                                        </label>
                                    </div>

                                <?php endfor ?>
                            </div>
                            <div class="col-md-2">
                                <?php
                                for ($i = 4; $i < 8; $i++) : ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="item_agenda[<?= $i ?>]"
                                                <?php if ($val_agenda[$i] == '1') : ?> value="1" checked <?php endif ?> />
                                            <?= $item[$i] ?>
                                        </label>
                                    </div>

                                <?php endfor ?>
                            </div>

                        </div>
                        <br>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- /.box -->
            </div>


            <div class="col-md-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Evaluasi Ibadah</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <form action="<?= base_url("laporan/simpan_evaluasi?kel=$_GET[kel]") ?>" method="POST">
                            <table id="table-laporan" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>X</th>
                                    <?php foreach ($binaan as $bin) : ?>
                                        <th>
                                            <center><?= $bin['nama_binaan'] ?></center>
                                        </th>
                                    <?php endforeach ?>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 0;
                                $j = 0;
                                foreach ($aktivitas as $key => $akt) : ?>
                                    <tr>
                                        <td><?= $akt['nama_aktivitas'] ?></td>
                                        <?php $j = 0;
                                        foreach ($binaan as $bin) : ?>
                                            <td>
                                                <?php if (isset($_GET['lap'])) : ?>
                                                    <center>
                                                        <input type="number" style="width: 50px"
                                                               value="<?= $mutabaah[$bin['id_binaan']][$akt['id_aktivitas']] ?>"/>
                                                    </center>
                                                <?php else : ?>
                                                    <center>
                                                        <input type="number" style="width: 50px" value="0"
                                                               name="eval[<?= $bin['id_binaan'] ?>][<?= $akt['id_aktivitas'] ?>]"/>
                                                    </center>
                                                <?php endif ?>
                                            </td>
                                        <?php endforeach ?>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                </form>
            </div>
            <!-- /.col-md-12 -->

</div>
</section>
<!-- /.Main content  -->

</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<?php $this->load->view('script'); ?>
<script type="text/javascript">
    $("#tgl-create-laporan").datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
</body>
</html>