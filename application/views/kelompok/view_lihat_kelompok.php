<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">

<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kelompok
                <small>Lihat Kelompok</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Kelompok</a></li>
                <li class="active">Lihat</li>
            </ol>
        </section>

        <!-- modal error -->
        <div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"> Warning</h4>
                        </div> -->

                    <div class="modal-body">
                        <center><h3>Mohon maaf, fitur ini masih dalam pengembangan. Mohon doanya :)</h3></center>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- end of modal error -->


        <!-- Main content -->
        <section class="content">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $title; ?></h3>

                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip"
                                    title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="table-kelompok" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kelompok</th>
                                <th>Fakultas</th>
                                <th>Angkatan</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;
                            foreach ($kelompok as $value) : ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value['nama_kelompok']; ?></td>
                                    <td><?php echo $value['fakultas_binaan']; ?></td>
                                    <td><?php echo $value['angkatan_binaan']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url("kelompok/detail?kel=$value[nama_kelompok]"); ?>"><i
                                                class="fa fa-eye"></i></a>&nbsp&nbsp
                                         <a href="<?php echo base_url("laporan?kel=$value[nama_kelompok]&m=$value[id_murabbi]"); ?>"><i class="fa fa-bar-chart-o"/></i></a>&nbsp&nbsp
                                        <!-- <a href="#" data-toggle="modal" data-target="#error-modal"><i
                                                class="fa fa-bar-chart-o"/></i></a>&nbsp&nbsp -->
                                        <a href="<?php echo base_url("kelompok/delete?kel=$value[nama_kelompok]"); ?>"
                                           onclick="return confirm('Apakah Anda yakin akan menghapus kelompok ini?');"><i
                                                class="fa fa-trash-o"/></i></a>&nbsp&nbsp
                                    </td>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col-md-12 -->
        </section>
        <!-- /.Main content  -->

    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->

<?php $this->load->view('script');?>
</body>
</html>