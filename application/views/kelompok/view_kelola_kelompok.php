<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">

<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Pengelolaan Kelompok
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li>Kelompok</li>
                <li class="active">Kelola</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">


            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Kelola Kelompok</h3>

                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip"
                                    title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <!-- tools box -->

                    <!-- form start -->
                    <div class="row">
                        <div class="col-md-6">
                            <?php if (isset($is_success) && ($is_success)) { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    <b>Alert!</b> <?php echo $message; ?>
                                </div>
                            <?php } else if (isset($is_success)) { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    <b>Alert!</b> <?php echo $message; ?>
                                </div>
                            <?php } ?>

                            <!-- <form role="form"> -->
                            <?php echo form_open_multipart('kelompok/submit_kelompok', "class='col-md-8'"); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="file_materi">File Pengelolaan Kelompok</label>
                                    <input type="file" id="file" name="file">

                                    <p class="help-block" style="width:450px;">*Sebelum upload, silahkan download file
                                        format pengelolaan kelompok dengan format CSV disamping</p>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">Download File Format</label>&nbsp&nbsp
                                    <a href="<?php echo base_url() . "file_uploaded/format_upload_kelompok.csv" ?>">
                                        <button class="btn-sm btn-primary">Download</button>
                                    </a>

                                    <p class="help-block">Format file untuk mengelola kelompok</p>
                                </div>
                                <div class="box-footer" style="margin-top:40px;"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
</div>

</section>
<!-- /.Main content  -->

</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script');?>
</body>
</html>