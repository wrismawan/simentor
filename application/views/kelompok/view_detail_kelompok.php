<!DOCTYPE html>
<html>
<?php $this->load->view('head'); ?>
<body class="skin-blue">

<?php $this->load->view('header'); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <?php $this->load->view('sidebar'); ?>
    <?php $this->load->view('notif'); ?>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kelompok
                <small>Lihat Kelompok</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Kelompok</a></li>
                <li class="active">Lihat</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Anggota Kelompok</h3>

                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip"
                                    title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive col-md-4">
                        <table id="table-detail-kelompok" class="table">
                            <thead>
                            <tr>
                                <th>Nama Mentor&nbsp&nbsp:&nbsp <?php echo $kelompok[0]['nama_murabbi']; ?></th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Angkatan&nbsp:&nbsp<?php echo $kelompok[0]['angkatan_murabbi']; ?></th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th>Fakultas&nbsp&nbsp:&nbsp<?php echo $kelompok[0]['fakultas_murabbi']; ?></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <br>

                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Fakultas</th>
                                <th>Jurusan</th>
                                <th>Angkatan</th>
                                <th>No Telp</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;
                            foreach ($kelompok as $value) { ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $value['nama_binaan']; ?></td>
                                    <td><?php echo $value['fakultas_binaan']; ?></td>
                                    <td><?php echo $value['jurusan_binaan']; ?></td>
                                    <td><?php echo $value['angkatan_binaan']; ?></td>
                                    <td><?php echo $value['no_hp_binaan']; ?></td>
                                    <td>
                                        <a href="<?php echo base_url("binaan/edit/$value[id_binaan]"); ?>"><i
                                                class="fa fa-edit"/></i></a>&nbsp&nbsp
                                        <?php if ($value['is_binaan_promote'] == 0) : ?>
                                            <a href="<?php echo base_url("binaan/request_up/$value[id_binaan]"); ?>"><i
                                                    class="fa fa-level-up"/></i></a>&nbsp&nbsp
                                        <?php endif ?>
                                        <a href="<?php echo base_url("binaan/delete?kel=$_GET[kel]&bin=$value[id_binaan]"); ?>"
                                           onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i
                                                class="fa fa-trash-o"/></i></a>&nbsp&nbsp
                                        <?php if ($value['is_binaan_promote'] == 1) : ?>
                                            <button class="btn btn-warning btn-xs">pending</button>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col-md-12 -->
        </section>
        <!-- /.Main content  -->

    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php $this->load->view('script');?>
</body
</html>