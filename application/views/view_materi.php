<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body class="skin-blue">
    <?php include 'header.php'; ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <?php include 'sidebar.php'; include 'notif.php'; ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Materi
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Materi</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <?php if (($this->session->userdata('role')=='Admin Murobbi') or
                        ($this->session->userdata('role')=='Admin Mentor')) { ?>
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Upload Materi</h3>
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        <!-- tools box -->

                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-6">
                                <?php if (isset($is_success) && ($is_success)){ ?>
                                <div class="alert alert-success alert-dismissable">
                                    <i class="fa fa-check"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Alert!</b> <?php echo $message; ?>
                                </div>
                                <?php } else if (isset($is_success)) { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <i class="fa fa-ban"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <b>Alert!</b> <?php echo $message; ?>
                                </div>
                                <?php } ?>

                                <!-- <form role="form"> -->
                                <?php echo form_open_multipart('materi/upload',"class='col-md-8'"); ?>
                                <div class="box-body">

                                    <div class="form-group">
                                        <label for="nama_materi">Nama Materi</label>
                                        <input type="text" class="form-control" name="nama_materi" id="nama_materi" placeholder="Masukkan Nama Materi">
                                    </div>
                                    <div class="form-group">
                                        <label for="file">File Materi</label>
                                        <input type="file" id="file" name="file" >
                                        <p class="help-block">Format file : ppt, pptx, doc, docx, xls, xlsx</p>
                                    </div>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                </div><!-- /.box -->
            </div>
            <?php } ?>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Materi</h3>  
                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->                                  
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="table-kelompok" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Materi</th>
                                    <th>Tingkatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($materi as $row) { ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row['nama_materi']; ?></td>
                                    <td><?php echo "Mentoring-$row[tingkatan]"; ?></td>
                                    <td><a href="<?php echo base_url("$row[path]"); ?>"><i class="fa fa-download"/></i></a>&nbsp&nbsp
                                    <?php $role = $this->session->userdata('role');
                                        if (($role == 'Admin Murobbi') or ($role == 'Admin Mentor')){ ?>
                                            <a href="<?php echo base_url("materi/delete/$row[id_materi]"); ?>"  onclick="return confirm('Apakah Anda yakin akan menghapus materi ini?')"><i class="fa fa-trash-o"/></i></a>
                                    <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div> <!-- /.col-md-12 -->
        </div>
    </section>
    <!-- /.Main content  -->

</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->
<?php include 'script.php'; ?>
</body>
</html>