<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body class="skin-blue">
    <?php include 'header.php'; ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        <?php include 'sidebar.php'; include 'notif.php'; ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Request
                    <small>Daftar Pending Requst</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Request</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Daftar Pending Request</h3>  
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->                                  
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="table-kelompok" class="table table-bordered table-striped">
                                <thead >
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Fakultas</th>
                                        <th>Angkatan</th>
                                        <th>Asal Kelompok</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($request as $value) : ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$value['nama_binaan']?></td>
                                        <td><?=$value['fakultas_binaan']?></td>
                                        <td><?=$value['angkatan_binaan']?></td>
                                        <td><?=$value['nama_kelompok']?></td>
                                        <td>
                                            <a href="<?=base_url("request/approve?req=$value[id_request]&bin=$value[id_binaan]")?>">
                                                <button class="btn btn-success btn-xs">Accept</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div> <!-- /.col-md-12 -->
            </section>
            <!-- /.Main content  -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->

    <!-- add new calendar event modal -->
    <?php include 'script.php'; ?>
</body>
</html>