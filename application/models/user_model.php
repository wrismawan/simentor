<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
	function get_password($id_user){
		$this->db->select('password');
		$query = $this->db->get_where('user', array('id_user'=>$id_user));
		$x = $query->result_array();
		return $x[0]['password'];
	}

	function save_new_password($id_user, $new_password){
		$this->db->where('id_user',$id_user);
		$this->db->update('user', array('password'=>$new_password));
	}

	function get_userdata($username, $password){
		$sql = "SELECT * FROM user WHERE username = '".$username."' and password = md5('".$password."') and is_delete = 0";
		$query = $this->db->query($sql);
		$x =  $query->result_array();
		return $x[0];
	}

	function get_username($username){
		$sql = "SELECT username FROM user WHERE username = '".$username."'";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0];
	}

	function get_nama(){
		$sql = "SELECT Nama FROM user";
		$query = $this->db->query($sql);
		return $query->result_array();

	}

	function get_id_user($username = '', $nama){
		if ($username == '')
			$where = " nama = '".$nama."'";
		 else 
			$where = " username = '".$username."' and nama = '".$nama."'";

		$sql = "SELECT id_user FROM user WHERE $where";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['id_user'];

	}

	function get_user($id_user){
		$query = $this->db->get_where('user', array('id_user'=>$id_user));
		$x = $query->result_array();
		return $x[0];
	}

	function get_all_user(){
		$this->db->order_by('Nama','ASC');
		$query = $this->db->get_where('user', array('is_delete'=>0));
		return $query->result_array();
	}

	function get_penerima_pesan($current_user){
		$sql = "SELECT * FROM user WHERE id_user <> $current_user and is_delete = 0 ORDER BY Nama ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function simpan($data){
		return $this->db->insert('user', $data);
	}

	function update($data){
		$this->db->where('id_user', $data['id_user']);
		$this->db->update('user',$data);
	}

	function delete($id_user){
		$this->db->where('id_user', $id_user);
		$this->db->update('user',array('is_delete' => 1));	
	}
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */