<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fakultas_model extends CI_Model {
	function get_all(){
		$query = $this->db->get('fakultas');
		return $query->result_array();
	}
}

/* End of file fakultas_model.php */
/* Location: ./application/models/fakultas_model.php */