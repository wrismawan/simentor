<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arsip_model extends CI_Model {

	function get($tingkatan){
		$sql = "SELECT * from arsip where tingkatan = $tingkatan";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function simpan_binaan($data){
		return $this->db->insert('arsip', $data);
	}

}

/* End of file arsip_model.php */
/* Location: ./application/models/arsip_model.php */