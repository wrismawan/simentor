<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_model extends CI_Model {	
	function get_laporan($nama_kelompok){
		$query = $this->db->get_where('laporan', array('nama_kelompok'=>$nama_kelompok));
		return $query->result_array();
	}

	function simpan_agenda($data){
		$this->db->insert('laporan',$data);
	}

	function get_latest_id($nama_kelompok){
		$sql = "SELECT MAX(id_laporan) as id FROM laporan WHERE nama_kelompok = '".$nama_kelompok."'";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['id'];
	}

	function get_agenda($id_laporan){
		$query = $this->db->get_where('laporan',array('id_laporan'=>$id_laporan));
		$x = $query->result_array();
		return $x[0];
	}

	function get_evaluasi($id_laporan, $id_binaan){
		$query = $this->db->get_where('evaluasi', array('id_laporan'=>$id_laporan, 'id_binaan'=>$id_binaan));
		$x = $query->result_array();
		if ($x)
			return $x[0];
		else 
			return false;
	}

	#--------- EVALUASI
	function get_evaluasi_binaan($id_laporan, $id_binaan){
		$sql = "SELECT e.*, a.nama_aktivitas 
		FROM evaluasi e, aktivitas a 
		WHERE e.id_laporan = $id_laporan and e.id_binaan = $id_binaan and a.id_aktivitas = e.id_aktivitas";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function get_mutabaah($id_lapooran){
        return $this->db->get_where('evaluasi', array('id_laporan'=>$id_lapooran))->result_array();
    }

	function simpan_evaluasi($data){
		return $this->db->insert('evaluasi',$data);
	}
	
}

/* End of file laporan_model.php */
/* Location: ./application/models/laporan_model.php */