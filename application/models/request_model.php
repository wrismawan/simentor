<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_model extends CI_Model {

	function get_request(){
		$sql = "SELECT r.*, k.* FROM request r, kelompok k WHERE r.id_binaan = k.id_binaan and is_approve = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}	

	function approve_request($id_request){
		$sql = "UPDATE request SET is_approve = 1 WHERE id_request = $id_request";
		return $this->db->query($sql);
	}

	function get_binaan($id_request){
		$sql = "SELECT id_binaan FROM request WHERE id_request = $id_request";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0];
	}

	function get_count_pending(){
		$sql = "SELECT COUNT(id_request) as count FROM request WHERE is_approve = 0";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['count'];
	}

	function simpan_request($data){
		return $this->db->insert('request', $data);
	}
}

/* End of file request_model.php */
/* Location: ./application/models/request_model.php */