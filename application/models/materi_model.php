<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materi_model extends CI_Model {
	function simpan($data){
		return $this->db->insert('materi', $data);
	}

	function get_all($tingkatan){
		$query = $this->db->get_where('materi',array('tingkatan'=>$tingkatan, 'is_delete'=>0));
		return $query->result_array();
	}

	function delete($id_materi){
		$this->db->where('id_materi', $id_materi);
		return $this->db->update('materi',array('is_delete'=>1));
	}
}

/* End of file materi_model.php */
/* Location: ./application/models/materi_model.php */