<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktivitas_model extends CI_Model {
	function get_aktivitas(){
		$this->db->where('is_delete',0);
		$query = $this->db->get('aktivitas');
		return $query->result_array();
	}

	function simpan($data){
		return $this->db->insert('aktivitas',$data);
	}

	function delete($id_aktivitas){
		$this->db->where('id_aktivitas', $id_aktivitas);
		$this->db->update('aktivitas', array('is_delete'=>1));
	}
}

/* End of file aktivitas_model.php */
/* Location: ./application/models/aktivitas_model.php */