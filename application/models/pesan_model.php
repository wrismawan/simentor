<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesan_model extends CI_Model {
	function simpan($data){
		return $this->db->insert('pesan',$data);
	}

	function status_read($id_thread, $val){
		$this->db->where('id_thread', $id_thread);
		$this->db->update('pesan', array('is_read'=>$val));
	}

	function get_latest_thread(){
		$sql = "SELECT MAX(id_thread) as id FROM pesan";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['id'];
	}

	function get_inbox($penerima){
		$sql = "SELECT p.*, u.nama FROM pesan p, user u WHERE u.id_user = p.id_penerima and judul_pesan <> '' and (p.id_penerima = $penerima or p.id_pengirim = $penerima) ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_id_thread($penerima){
		$sql = "SELECT DISTINCT id_thread, id_pengirim, id_penerima, is_delete_penerima, is_delete_pengirim, is_read FROM pesan WHERE (id_penerima = $penerima or id_pengirim = $penerima) and judul_pesan <> ''";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_owner($id_thread){
		$sql = "SELECT id_penerima, id_pengirim FROM pesan WHERE id_thread = $id_thread";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0];
	}

	function get_thread($id_thread){
		$sql = "SELECT u.nama, p.* FROM pesan p, user u WHERE u.id_user = p.id_pengirim and id_thread = $id_thread ORDER BY tanggal ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_count_unread($id_user){
		$sql = "SELECT COUNT(DISTINCT id_thread) as count
				FROM pesan 
				WHERE ($id_user = id_pengirim or $id_user = id_penerima) and is_read = 0";

		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['count'];
	}

	function delete($data){
		if ($data['mode'] == 'penerima'){
			$this->db->where('id_penerima',$data['id_user']);
			$update['is_delete_penerima'] = 1;
		}
		else {
			$this->db->where('id_pengirim',$data['id_user']);
			$update['is_delete_pengirim'] = 1;
		}
		$this->db->update('pesan', $update);
	}

	function reset_delete($id_thread){
		$update = array ('is_delete_pengirim'=>0, 'is_delete_penerima'=>0);
		$this->db->where('id_thread', $id_thread);
		$this->db->update('pesan', $update);
	}
}

/* End of file pesan_model.php */
/* Location: ./application/models/pesan_model.php */