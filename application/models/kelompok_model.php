<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelompok_model extends CI_Model {
	#========= Kelompok ===========	
	function simpan($data){
		return $this->db->insert('kelompok',$data);
	}

	function count_kelompok_tingkat1(){
		$sql = "SELECT COUNT(DISTINCT nama_kelompok) as count FROM kelompok WHERE is_kelompok_deleted = 0 and tingkatan = 1";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['count'];
	}

	function count_kelompok_tingkat2(){
		$sql = "SELECT COUNT(DISTINCT nama_kelompok) as count FROM kelompok WHERE is_kelompok_deleted = 0 and tingkatan = 2";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['count'];
	}

	function count_murobbi(){
		$sql = "SELECT COUNT(id_murabbi) as count FROM kelompok";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['count'];
	}

	function update($data){
		$this->db->where('id_binaan', $data['id_binaan']);
		$this->db->update('kelompok', $data);
	}

	function get_daftar_kelompok($id_murabbi){
		$sql = "SELECT DISTINCT id_murabbi, nama_kelompok, fakultas_binaan, angkatan_binaan FROM kelompok WHERE is_kelompok_deleted = 0 and id_murabbi = $id_murabbi";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_daftar_kelompok_for_admin($tingkatan){
		$sql = "SELECT DISTINCT id_murabbi, nama_kelompok, fakultas_binaan, angkatan_binaan FROM kelompok WHERE is_kelompok_deleted = 0 and tingkatan = $tingkatan";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_detail_kelompok($nama_kelompok){
		$sql = "SELECT * FROM kelompok WHERE nama_kelompok = '".$nama_kelompok."' and is_binaan_deleted = 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function delete_kelompok($nama_kelompok){
		$this->db->where('nama_kelompok', $nama_kelompok);
		$this->db->update('kelompok', array('is_kelompok_deleted'=>1));
		
	}

	#======= Binaan ========
	function get_binaan($id_binaan){
		$sql = "SELECT id_binaan, nama_binaan, nama_kelompok, fakultas_binaan, jurusan_binaan, angkatan_binaan, no_hp_binaan, tingkatan FROM kelompok WHERE id_binaan = $id_binaan and is_binaan_deleted = 0";
		$query = $this->db->query($sql);
		$x = $query->result_array();	
		return $x[0];
	}

	function get_latest_id_binaan(){
		$sql = "SELECT MAX(id_binaan) as id FROM kelompok";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['id'];
	}

	function get_binaan_by_kelompok($nama_kelompok){
		$sql = "SELECT id_binaan, nama_binaan FROM kelompok WHERE is_binaan_deleted = 0 and nama_kelompok = '".$nama_kelompok."' ORDER BY nama_binaan ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function level_up($id_binaan){
		var_dump($id_binaan);
		$sql = "UPDATE kelompok SET tingkatan = tingkatan + 1 WHERE id_binaan = $id_binaan";
		return $query = $this->db->query($sql);
	}

	function update_promote($id_binaan){
		$this->db->where('id_binaan', $id_binaan);
		$this->db->update('kelompok', array('is_binaan_promote'=>1));
	}

	function get_tingkatan($id_binaan){
		$sql = "SELECT tingkatan FROM kelompok WHERE id_binaan = $id_binaan";
		$query = $this->db->query($sql);
		$x = $query->result_array();
		return $x[0]['tingkatan'];
	}

	function get_nama_kelompok($id_binaan){
		$sql = "SELECT nama_kelompok FROM kelompok WHERE id_binaan = $id_binaan";
		$query = $this->db->query($sql);
		$x = $query->result_array();	
		return $x[0];		
	}

	function delete_binaan($id_binaan){
		$this->db->where('id_binaan', $id_binaan);
		$this->db->update('kelompok', array('is_binaan_deleted'=>1));
	}

}

/* End of file kelompok_model.php */
/* Location: ./application/models/kelompok_model.php */